﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Text;


namespace HW5_REFLECTION
{
    class MyCustomSerializer
    { private static string ListSeparator = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ListSeparator;

        public static string SerializeToCsv(object obj)
        {
            var type = obj.GetType();

            var sb = new StringBuilder();
            sb.AppendLine(type.FullName);

            foreach (var field in type.GetFields(System.Reflection.BindingFlags.Public))
            {
                sb.AppendLine($"{field.Name}{ListSeparator}{field.GetValue(obj)}");
            }

            return sb.ToString();
        }

        public static object DeserializeFromCsv(string csv)
        {
            var lines = csv.Split("\r\n").Where(line => !string.IsNullOrEmpty(line)).ToArray();

            var typeName = lines[0];
            var handle = Activator.CreateInstance(null, typeName);
            var obj = handle?.Unwrap();
                        
            if (obj == null)
                return null;

            for (var i = 1; i < lines.Length; i++)
            {
                LoadFields(lines[i], obj);
            }

            return obj;
        }

        private static void LoadFields(string field, object obj)
        {
            var parts = field.Split(ListSeparator);
            var type = obj.GetType();
            var fieldInfo = type.GetField(parts[0]);
            fieldInfo.SetValue(obj, int.Parse(parts[1]));
        }

    }
}
